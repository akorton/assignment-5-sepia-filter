SOLUTION_DIR = solution
SRC_DIR = $(SOLUTION_DIR)/src
TESTER_DIR = tester
SRC_TESTER_DIR = $(TESTER_DIR)/src

OBJECT_DIR = obj
OBJECT_TESTER_DIR = $(OBJECT_DIR)/tester
BUILD_DIR = build

INCLUDE_TESTER_DIR = $(TESTER_DIR)/include
TESTS_TESTER_DIR = $(TESTER_DIR)/tests

dir_builder=@mkdir -p $(@D)
CC = gcc
LINKER = $(CC)
ASM = nasm
CFLAGS = -o2
ASMFLAGS = -felf64

all: $(BUILD_DIR)/main


$(BUILD_DIR)/main: $(OBJECT_DIR)/bmp.o $(OBJECT_DIR)/image.o $(OBJECT_DIR)/main.o $(OBJECT_DIR)/transform.o $(OBJECT_DIR)/transform_pixel.o $(OBJECT_DIR)/utils.o
	$(dir_builder)
	$(LINKER) -g -o $@ $^

$(OBJECT_DIR)/%.o: $(SRC_DIR)/%.c
	$(dir_builder)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJECT_DIR)/transform_pixel.o: $(SRC_DIR)/transform_pixel.asm
	$(dir_builder)
	$(ASM) $(ASMFLAGS) -o $@ $<


tester: $(BUILD_DIR)/tester

$(BUILD_DIR)/tester: $(OBJECT_TESTER_DIR)/main.o $(OBJECT_TESTER_DIR)/bmp.o $(OBJECT_TESTER_DIR)/file_cmp.o $(OBJECT_TESTER_DIR)/util.o
	$(dir_builder)
	$(LINKER) -g -o $@ $^

$(OBJECT_TESTER_DIR)/%.o: $(SRC_TESTER_DIR)/%.c
	$(dir_builder)
	$(CC) $(CFLAGS) -I $(INCLUDE_TESTER_DIR) -c $< -o $@


.PHONY: test

test: tester all
	for number in 1 2 3 ; do \
		./build/main $(TESTS_TESTER_DIR)/$$number/input.bmp $(TESTS_TESTER_DIR)/$$number/output.bmp $(mode) 100;	\
		./build/tester $(TESTS_TESTER_DIR)/$$number/output.bmp $(TESTS_TESTER_DIR)/$$number/output_expected.bmp; \
	done
