#include "../include/bmp.h"

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    size_t res = fread(&header, 1, sizeof(struct bmp_header), in);
    if (res != sizeof(struct bmp_header)){
        return READ_INVALID_HEADER;
    } else if (header.bfType != 0x4d42 && header.bfType != 0x4349 && header.bfType != 0x5450){
        return READ_INVALID_TYPE;
    } else if (header.biBitCount != 24){
        return READ_INVALID_BIT_COUNT;
    } else if (header.biCompression != 0){
        return READ_COMPRESSION_ERROR;
    } else if (header.biSize != 40 && header.biSize != 108 && header.biSize != 124){
        return READ_BI_SIZE_ERROR;
    } else if (header.biPlanes != 1){
        return READ_BI_PLANES_ERROR;
    }
    if (header.bOffBits > sizeof(struct bmp_header)) {
        if (fseek(in, (long) (header.bOffBits - sizeof(struct bmp_header)), SEEK_CUR)) {
            return READ_INVALID_BOFF_BITS;
        }
    }
    enum create_result create_res = create_image(header.biWidth, header.biHeight, img);
    if (create_res == CREATE_WRONG) return READ_CREATE_IMAGE_ERROR;
    size_t row_size = (header.biWidth*3 + 3) & (-4);
    uint8_t* data = malloc(row_size * header.biHeight);
    if (data == NULL) return READ_MALLOC_DATA_ERROR;
    res = fread(data, 1, row_size*header.biHeight, in);
    if (res != (row_size*header.biHeight)){
        free(data);
        free(img);
        return READ_DATA_ERROR;
    }
    for (size_t j = 0;j < header.biHeight;j++){
        for (size_t i = 0; i < header.biWidth;i++){
            struct pixel cur_pxl = img->data[j*header.biWidth + i];
            cur_pxl.r = data[j*row_size+3*i + 2];
            cur_pxl.g = data[j*row_size+3*i + 1];
            cur_pxl.b = data[j*row_size+3*i];
            img->data[j*header.biWidth + i] = cur_pxl;
        }
    }
    free(data);
    return READ_OK;
}

enum check_status print_read_result(enum read_status read_res){
    if (read_res == READ_INVALID_HEADER){
        printf("Can not read header.\n");
        return RETURN;
    }
    if (read_res == READ_DATA_ERROR){
        printf("Can not read data from bmp file.\n");
        return RETURN;
    } 
    if (read_res == READ_MALLOC_DATA_ERROR){
        printf("Can not malloc enough memory for data.\n");
        return RETURN;
    }
    if (read_res == READ_INVALID_TYPE){
        printf("Invalid type of bmp file.\n");
        return RETURN;
    }
    if (read_res == READ_INVALID_BIT_COUNT){
        printf("Wrong bmp file bit count.\n");
        return RETURN;
    }
    if (read_res == READ_COMPRESSION_ERROR){
        printf("File is compressed.\n");
        return RETURN;
    }
    if (read_res == READ_BI_SIZE_ERROR){
        printf("Invalid biSize parameter.\n");
        return RETURN;
    }
    if (read_res == READ_BI_PLANES_ERROR){
        printf("Invalid biPlanes parameter.\n");
        return RETURN;
    }
    if (read_res == READ_CREATE_IMAGE_ERROR){
        printf("Error occured while creating image.\n");
        return RETURN;
    }
    if (read_res == READ_INVALID_BOFF_BITS){
        printf("Invalid bOffBits parameter.\n");
        return RETURN;
    }
    printf("Input BMP file is ok, starting to transform!\n");
    return CONTINUE;
}

void create_header(struct bmp_header* header, struct image const* img){
    size_t row_size = (3*img->width + 3) & (-4);
    header->bfType = DEFAULT_BF_TYPE;
    header->bfileSize = sizeof(struct bmp_header) + row_size*img->height;
    header->bfReserved = DEFAULT_BF_RESERVED;
    header->biPlanes = DEFAULT_BI_PLANES;
    header->biSize = DEFAULT_BI_SIZE;
    header->bOffBits = DEFAULT_B_OFF_BITS;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biBitCount = DEFAULT_BI_COUNT;
    header->biCompression = DEFAULT_BI_COMPRESSION;
    header->biSizeImage = DEFAULT_BI_SIZE_IMAGE;
    header->biXPelsPerMeter = DEFAULT_BI_X_PELS_PER_METER;
    header->biYPelsPerMeter = DEFAULT_BI_Y_PELS_PER_METER;
    header->biClrImportant = DEFAULT_BI_CLR_IMPORTANT;
    header->biClrUsed = DEFAULT_BI_CLR_USED;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header;
    create_header(&header, img);
    size_t row_size = (3*img->width + 3) & (-4);

    size_t res = fwrite(&header, 1, sizeof(struct bmp_header), out);
    if (res != sizeof(struct bmp_header)){
        return WRITE_HEADER_ERROR;
    }

    uint8_t* data = malloc(row_size*img->height);
    if (data == NULL) return WRITE_MALLOC_DATA_ERROR;
    size_t padding = row_size - 3*img->width;
    for (size_t j = 0; j < img->height;j++){
        for (size_t i = 0; i < img->width;i++){
            struct pixel cur_pxl = img->data[j*img->width+i];
            data[j*row_size + i*3] = cur_pxl.b;
            data[j*row_size+i*3+1] = cur_pxl.g;
            data[j*row_size+i*3+2] = cur_pxl.r;
        }
        for (size_t i = 0; i < padding;i++) data[j*row_size + 3*img->width + i] = 0; // no use-of-uninitialized-value warning. 
                                                                                // It's just padding but its nicer without a warrning :)
    }
    res = fwrite(data, 1, row_size*img->height, out);
    free(data);
    if (res != (row_size*img->height)){
        return WRITE_DATA_ERROR;
    }
    return WRITE_OK;
}
