#include "../include/utils.h"
#include "../include/bmp.h"
#include "../include/transform.h"
#include <time.h>
#include <stdlib.h>
#include <string.h>


int main( int argc, char** argv ) {
    printf("Cur number of arguments is %i.\n", argc);
    if (argc < 4) {
        printf("Usage: ./main INPUT.bmp OUTPUT.bmp mode NUMBER_OF_ITERATIONS\nMode can be 'c' or 'asm'\n");
        return 0;
    }
    FILE* file_in_ptr;
    enum open_result open_result_in = open_file(argv[1], "r", &file_in_ptr);
    if (open_result_in == OPEN_WRONG){
        printf("Can't open %s file.\n", argv[1]);
        return 0;
    }
    printf("File %s successfully opened!\n", argv[1]);

    struct image img;
    enum read_status read_res = from_bmp(file_in_ptr, &img);
    enum check_status status = print_read_result(read_res);
    enum close_result close_result_in = close_file(file_in_ptr);
    if (status == RETURN){
        return 0;
    }
    if (close_result_in == CLOSE_WRONG){
        printf("Can't close %s file.\n", argv[1]);
        free_image(&img);
        return 0;
    }
    printf("File %s successfully closed!\n", argv[1]);

    char* mode = argv[3];
    struct image rotated_img;
    enum rotate_result rotate_res;
    clock_t start_c, end_c, start_asm, end_asm;
    int n = 1;
    if (argc >= 5){
        n = atoi(argv[4]);
    }
    if (strcmp(mode, "c") == 0){
    start_c = clock();
        for (int i = 0; i < n;i++){
            if (i != 0) free_image(&rotated_img);
            rotate_res = sepia_c(img, &rotated_img);
        }
        end_c = clock();
    } else if (strcmp(mode, "asm") == 0){
        start_asm = clock();
        for (int i = 0; i < n;i++){
            if (i != 0) free_image(&rotated_img);
            rotate_res = sepia_asm(img, &rotated_img);
        }
        end_asm = clock();
    } else {
        printf("Current mode is not supported!\nPossible mode values are 'c' and 'asm'.\n");
        return 0;
    }

    if (rotate_res == ROTATE_CREATE_IMAGE_ERROR){
        free_image(&img);
        printf("Can not create image during rotate.\n");
        return 0;
    }
    printf("Successfully transformed!\n");
    free_image(&img);

    FILE* file_out_ptr;
    enum open_result open_result_out = open_file(argv[2], "wb", &file_out_ptr);
    if (open_result_out == OPEN_WRONG){
        printf("Can't open %s file.\n", argv[2]);
        return 0;
    }
    printf("File %s successfully opened!\n", argv[2]);

    enum write_status write_result = to_bmp(file_out_ptr, &rotated_img);
    free_image(&rotated_img);
    enum close_result close_result_out = close_file(file_out_ptr);
    if (write_result == WRITE_HEADER_ERROR){
        printf("Can not write header.\n");
        return 0;
    }
    if (write_result == WRITE_DATA_ERROR){
        printf("Can not write data.\n");
        return  0;
    }
    if (write_result == WRITE_MALLOC_DATA_ERROR){
        printf("Can not malloc enough memory for data.\n");
        return 0;
    }
    printf("Writed successfully!\n");
    if (close_result_out == CLOSE_WRONG){
        printf("Can't close %s file.\n", argv[2]);
        return 0;
    }
    printf("File %s successfully closed!\n", argv[2]);
    printf("Number of iterations: %i.\n", n);
    if (strcmp(mode, "c") == 0) printf("C function worked: %lf.\n", ((double) (end_c - start_c)) / CLOCKS_PER_SEC);
    if (strcmp(mode, "asm") == 0) printf("Asm function worked: %lf.\n\n", ((double) (end_asm - start_asm)) / CLOCKS_PER_SEC);
    return 0;
}
