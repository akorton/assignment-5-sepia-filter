#include "../include/image.h"

enum create_result create_image(uint64_t width, uint64_t height, struct image* img_ptr){
    img_ptr->width = width;
    img_ptr->height = height;
    img_ptr->data = malloc(sizeof(struct pixel) * height * width);
    if (img_ptr->data == NULL) return CREATE_WRONG;
    return CREATE_OK;
}

void free_image(struct image* img){
    free(img->data);
}

void set_data(struct image* img, struct pixel* data){
    img->data = data;
}
