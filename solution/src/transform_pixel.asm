
 


 %define matrix_shuffle_pattern 0b01111001
%define cycle_counter 3

%macro load 0
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2

    pinsrb xmm0, [rdi], 0; xmm0[0] = b[i]
    pinsrb xmm1, [rdi+1], 0; xmm1[0] = g[i]
    pinsrb xmm2, [rdi+2], 0; xmm2[0] = r[i]

    pinsrb xmm0, [rdi+3], 12; xmm0[12] = b[i+1]
    pinsrb xmm1, [rdi+4], 12; xmm1[12] = g[i+1]
    pinsrb xmm2, [rdi+5], 12; xmm2[12] = r[i+1]
%endmacro

%macro write 0
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12
%endmacro

%macro next 0
    add rdi, 3; go to next source pixel
    add rsi, 4; go to next 4 transormed float values

    shufps xmm3, xmm3, matrix_shuffle_pattern
    shufps xmm4, xmm4, matrix_shuffle_pattern
    shufps xmm5, xmm5, matrix_shuffle_pattern
%endmacro

%macro shuffle 1
    %if %1 == 1
        %define registers_shuffle_pattern 0b11000000
    %elif %1 == 2
        %define registers_shuffle_pattern 0b11110000
    %elif %1 == 3
        %define registers_shuffle_pattern 0b11111100
    %else 
        %error "Unsupported case"
    %endif


    shufps xmm0, xmm0, registers_shuffle_pattern
    shufps xmm1, xmm1, registers_shuffle_pattern
    shufps xmm2, xmm2, registers_shuffle_pattern
%endmacro

%macro setup 0
    mov r8, blue_matrix
    movdqa xmm3, [r8]
    mov r8, green_matrix
    movdqa xmm4, [r8]
    mov r8, red_matrix
    movdqa xmm5, [r8]
%endmacro

%macro transform 0
    
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0

    mov r8, max_matrix
    pminsd xmm0, [r8]
%endmacro

%macro main 1
    load
    shuffle %1
    transform
    write
    next
%endmacro



section .rodata
align 16
blue_matrix: dd 0.131, 0.168, 0.189, 0.131
align 16
green_matrix: dd 0.534, 0.686, 0.769, 0.534
align 16
red_matrix: dd 0.272, 0.349, 0.393, 0.272
align 16
max_matrix: dd 0xff, 0xff, 0xff, 0xff

section .text
global transform_pixel_asm
transform_pixel_asm:
    setup
    main 1
    main 2
    main 3
    ret
