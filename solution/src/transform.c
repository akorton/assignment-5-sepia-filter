#include "../include/transform.h"

uint64_t get_result_pixel(uint64_t width, size_t i, size_t j){
    return j*width+i;
}

uint64_t get_source_pixel(uint64_t width, size_t i, size_t j){
    return j*width + i;
}

struct pixel transform(struct pixel source){
    return (struct pixel) {
        .r = min(MAX_VALUE, (int) (source.r*RED_RED + source.b*RED_BLUE + source.g*RED_GREEN)),
        .g = min(MAX_VALUE, (int) (source.r*GREEN_RED + source.b*GREEN_BLUE + source.g*GREEN_GREEN)),
        .b = min(MAX_VALUE, (int) (source.r*BLUE_RED + source.b*BLUE_BLUE + source.g*BLUE_GREEN))
    };
}

enum rotate_result sepia_c(struct image const source, struct image* rotated){
    enum create_result create_res = create_image(source.width, source.height, rotated);
    if (create_res == CREATE_WRONG) return ROTATE_CREATE_IMAGE_ERROR;
    for (size_t i = 0; i < source.width;i++){
        for (size_t j = 0; j < source.height; j++){
            rotated->data[get_result_pixel(rotated->width, i, j)] = transform(source.data[get_source_pixel(source.width, i, j)]);
        }
    }
    return ROTATE_OK;
}

extern void transform_pixel_asm(struct pixel* source, struct pixel* transformed);


enum rotate_result sepia_asm(struct image const source, struct image* rotated){
    enum create_result create_res = create_image(source.width, source.height, rotated);
    if (create_res == CREATE_WRONG) return ROTATE_CREATE_IMAGE_ERROR;
    size_t size = rotated->height * rotated->width;
    size_t i;
    for (i = 0; i + 4 < size;i+=4){
        transform_pixel_asm(source.data + i, rotated->data+i);
    }
    for (i; i < size;i++){
        rotated->data[i] = transform(source.data[i]);
    }
    return ROTATE_OK;
}
