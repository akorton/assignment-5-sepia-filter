#pragma once
#ifndef UTILS_H_LAB5
#define UTILS_H_LAB5

#include <stdio.h>

enum open_result{
    OPEN_OK = 0,
    OPEN_WRONG
};

enum open_result open_file(const char* filename, const char* mode, FILE** file_ptr_ptr);

enum close_result{
    CLOSE_OK = 0,
    CLOSE_WRONG
};

enum close_result close_file(FILE* fileptr);
#endif
