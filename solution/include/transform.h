#pragma once
#ifndef TRANSFORM_H_LAB5
#define TRANSFORM_H_LAB5

#include "image.h"
#define min( x, y ) ( (x) > (y) ? (y) : (x) )
#define MAX_VALUE 255
#define RED_RED 0.393
#define RED_GREEN 0.769
#define RED_BLUE 0.189
#define GREEN_RED 0.349
#define GREEN_GREEN 0.686
#define GREEN_BLUE 0.168
#define BLUE_RED 0.272
#define BLUE_GREEN 0.534
#define BLUE_BLUE 0.131

enum rotate_result{
    ROTATE_OK = 0,
    ROTATE_CREATE_IMAGE_ERROR
};

enum rotate_result sepia_c(struct image const source, struct image* rotated);
enum rotate_result sepia_asm(struct image const source, struct image* rotated);
#endif
