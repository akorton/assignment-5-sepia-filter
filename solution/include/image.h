#pragma once
#ifndef IMAGE_H_LAB5
#define IMAGE_H_LAB5

#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

enum create_result{
  CREATE_OK = 0,
  CREATE_WRONG
};

enum create_result create_image(uint64_t width, uint64_t height, struct image* img_ptr);
void free_image(struct image* img);
void set_data(struct image* img, struct pixel* data);
#endif
