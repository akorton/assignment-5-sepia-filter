#pragma once
#ifndef BMP_H_LAB5
#define BMP_H_LAB5

#include "image.h"

enum DEFAULT_HEADER {
  DEFAULT_BF_TYPE = 0x4d42,
  DEFAULT_BF_RESERVED = 0,
  DEFAULT_BI_PLANES = 1,
  DEFAULT_BI_SIZE = 40,
  DEFAULT_B_OFF_BITS = ((DEFAULT_BI_SIZE) + 14),
  DEFAULT_BI_COUNT = 24,
  DEFAULT_BI_COMPRESSION = 0,
  DEFAULT_BI_SIZE_IMAGE = 0,
  DEFAULT_BI_X_PELS_PER_METER = 0,
  DEFAULT_BI_Y_PELS_PER_METER = 0,
  DEFAULT_BI_CLR_IMPORTANT = 0,
  DEFAULT_BI_CLR_USED = 0
};


struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_TYPE,
  READ_INVALID_BIT_COUNT,
  READ_INVALID_HEADER,
  READ_MALLOC_DATA_ERROR,
  READ_DATA_ERROR,
  READ_COMPRESSION_ERROR,
  READ_BI_SIZE_ERROR,
  READ_BI_PLANES_ERROR,
  READ_CREATE_IMAGE_ERROR,
  READ_INVALID_BOFF_BITS
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_DATA_ERROR,
  WRITE_MALLOC_DATA_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );
/*
RETURN - there was a mistake and we need to exit the programm
CONTINUE - everything is just fine lets rotate it completely!
*/
enum check_status {
  RETURN = 0,
  CONTINUE
};
enum check_status print_read_result(enum read_status read_res);
#endif
